export const preloadTemplates = async function() {
	const templatePaths = [
		// Add paths to "modules/minor-qol/templates"
		"modules/minor-qol/templates/saves.html",
		"modules/minor-qol/templates/hits.html",
		"modules/minor-qol/templates/spellleveldialog.html"
	];

	return loadTemplates(templatePaths);
}
